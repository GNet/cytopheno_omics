source("anova3F.R")
dir_script=getwd()

data = read.table("../data/data_metabolome_24ech.txt", sep="\t", dec=",",header=TRUE, row.names=1)
data = t(data)

## definition of the design
cyto<-rep(rep(c("Ct-1","Jea"),each=3),4)
nucleus<-rep(rep(c("Ct-1","Jea"),each=6),2)
nitrogen<-rep(c("N0","control"),each=12)
design<-cbind(cyto,nucleus,nitrogen)


dir.create("../Results_metabo")
setwd("../Results_metabo")
anova3F(data, design)
setwd(dir_script)


